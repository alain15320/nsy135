package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modeles.TestsHibernate;

/**
 * Servlet implementation class Hibernate
 */
@WebServlet("/hibernate")
public class Hibernate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String VUES="/vues/hibernate/";	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public Hibernate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		TestsHibernate tstHiber;
		String maVue = VUES + "connexion.jsp";
		
		try {
			tstHiber = new TestsHibernate();
		} catch (Exception e) {
			maVue = VUES + "exception.jsp";
			request.setAttribute("message", e.getMessage());
			System.out.println(e.getMessage());
		}

		
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
