package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class JspParam
 */
@WebServlet(
		urlPatterns = { "/JspParam" }, 
		initParams = { 
				@WebInitParam(name = "p1", value = "Azerty"), 
				@WebInitParam(name = "p2", value = "250"), 
				@WebInitParam(name = "p3", value = "3.1416")
		})
public class JspParam extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public JspParam() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String maVue = "/jspparam.jsp?p1=Azerty&p2=250&p3=2.65";
		String p1 = "Voltaire";
		request.getParameter(p1);
		
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
