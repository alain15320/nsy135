package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;
import java.util.List;
import java.util.Map;

import modeles.Artiste;
import modeles.Film;
import modeles.TestsJdbc;

/**
 * Servlet implementation class Jdbc
 */
@WebServlet("/jdbc")
public class Jdbc extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String SERVEUR="192.168.1.6";
	private static final String BD="webscope";
	private static final String LOGIN="orm";
	private static final String PASSWORD="orm";
	private static final String VUES="/vues/jdbc/";	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Jdbc() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// recuperation de l'action requise par l'utilisateur
		String action = request.getParameter("action");
		// Notre objet Modèle : accès à MySQL
		TestsJdbc jdbc;
		// La vue par defaut
		String maVue = VUES + "index.jsp";
		
		try {
			jdbc = new TestsJdbc();
			if (action == null) {
				// Rien à faire
			} else if (action.equals("connexion")) {
				// Action + vue test de connexion
				jdbc.connect(SERVEUR, BD, LOGIN, PASSWORD);
				maVue = VUES + "connexion.jsp";
			} else if (action.equals("requeteA")) {
				jdbc.connect(SERVEUR, BD, LOGIN, PASSWORD);
				ResultSet resultat = jdbc.chercheFilmA();
				request.setAttribute("films", resultat);
				maVue = VUES + "filmsA.jsp";
			} else if (action.equals("requeteB")) {
				jdbc.connect(SERVEUR, BD, LOGIN, PASSWORD);
				List<Map<String, String>> resultat = jdbc.chercheFilmB();
				request.setAttribute("films", resultat);
				maVue = VUES + "filmsB.jsp";
			} else if (action.equals("requeteC")) {
				jdbc.connect(SERVEUR, BD, LOGIN, PASSWORD);
				List<Film> resultat = jdbc.chercheFilmC();
				request.setAttribute("films", resultat);
				maVue = VUES + "filmsC.jsp";
			} else if (action.equals("artiste")) {
				jdbc.connect(SERVEUR, BD, LOGIN, PASSWORD);
				Integer idArtiste = Integer.valueOf(request.getParameter("id"));
				Artiste resultat = jdbc.chercheArtiste(idArtiste);
				request.setAttribute("artiste", resultat);
				maVue = VUES + "artiste.jsp";
			} else if (action.equals("requeteD")) {
				jdbc.connect(SERVEUR, BD, LOGIN, PASSWORD);
				List<Film> resultat = jdbc.chercheFilmD();
				request.setAttribute("films", resultat);
				maVue = VUES + "filmsD.jsp";
			}
		} catch (Exception e) {
			maVue = VUES + "exception.jsp";
			request.setAttribute("message", e.getMessage());
			System.out.println(e.getMessage());
		}
		
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
