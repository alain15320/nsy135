package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modeles.Temperature;

/**
 * Servlet implementation class Convertisseur
 */
@WebServlet("/Convertisseur")
public class Convertisseur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Convertisseur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String maVue = "/convinput.jsp";
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String valCelsius = request.getParameter("celsius");
		// String valeur="";
		System.out.println(valCelsius);
		
		if (valCelsius.isEmpty()) {
			valCelsius = "20";
		}
		
		
/*		String valUnite = request.getParameter("unite");
		if (valUnite=="C") {
			valeur = request.getParameter("celsius");
			if (valeur.isEmpty()) {
				valeur = "20";
			}
		} else if (valUnite=="F") {
			valeur = request.getParameter("fahrenheit");
			if (valeur.isEmpty()) {
				valeur = "70";
			}
		}*/
		

		
		Temperature temp = new Temperature(Double.valueOf(valCelsius));
		// Temperature temp = new Temperature(Double.valueOf(valeur), valUnite);
		// request.setAttribute("valeur", valeur);
		// request.setAttribute("unite", valUnite);
		request.setAttribute("temperature", temp);
		
		// Transfert à la vue
		String maVue = "/convoutput.jsp";
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);
	}

}
