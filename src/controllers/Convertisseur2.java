package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modeles.Temperature2;

/**
 * Servlet implementation class Convertisseur2
 */
@WebServlet("/Convertisseur2")
public class Convertisseur2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Convertisseur2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String maVue = "/convinput2.jsp";
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String valTemp="";
		String unite="";
		String maVue;
		
		if (request.getParameter("celsius")!="") {
			valTemp = request.getParameter("celsius");
			unite = "C";
			maVue = "/convoutput.jsp";
		} else {
			valTemp = request.getParameter("fahrenheit");
			unite = "F";
			maVue = "/convoutput2.jsp";
			System.out.print(valTemp);
		}
		
		Temperature2 temp = new Temperature2(Double.valueOf(valTemp), unite);
		request.setAttribute("temperature", temp);
		
		RequestDispatcher dispacher = getServletContext().getRequestDispatcher(maVue);
		dispacher.forward(request, response);
	}

}
