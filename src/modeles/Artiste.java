package modeles;

public class Artiste {
	
	private Integer id;
	private String nom;
	private String prenom;
	private Integer annee_naissance;
	
	public Artiste() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Integer getAnnee_naissance() {
		return annee_naissance;
	}

	public void setAnnee_naissance(Integer annee_naissance) {
		this.annee_naissance = annee_naissance;
	}

	@Override
	public String toString() {
		return "Artiste [nom=" + nom + ", prenom=" + prenom + "]";
	}

}
