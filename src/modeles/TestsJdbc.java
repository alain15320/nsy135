package modeles;

import java.sql.*;
import java.util.*;


public class TestsJdbc {

	private static final Integer port = 3306;
	private Connection connexion;
	
	public TestsJdbc() throws ClassNotFoundException {
		// On commence par charger le driver MySQL
		Class.forName("com.mysql.jdbc.Driver");
	}
	public void connect(String server, String bd, String user, String password) throws SQLException {
		String url = "jdbc:mysql://" + server + ":" + port + "/" + bd + "?autoReconnect=true&verifyServerCertificate=false&useSSL=false";
		connexion = DriverManager.getConnection(url, user, password);
	}

	public ResultSet chercheFilmA() throws SQLException {
		Statement statement = connexion.createStatement();
		ResultSet films = statement.executeQuery("SELECT * FROM Film");
		return films;
	}
	
	public List<Map<String, String>> chercheFilmB() throws SQLException {
		List<Map<String, String>> resultat = new ArrayList<Map<String, String>>();
		Statement statement = connexion.createStatement();
		ResultSet films = statement.executeQuery("SELECT * FROM Film");
		while (films.next()) {
			Map<String, String> film = new HashMap<String, String>();
			film.put("id", String.valueOf(films.getInt("id")));			
			film.put("titre", films.getString("titre"));
			film.put("annee", String.valueOf(films.getInt("annee")));
			film.put("genre", films.getString("genre"));
			resultat.add(film);
		}
		return resultat;
	}

	public List<Film> chercheFilmC() throws SQLException {
		List<Film> resultat = new ArrayList<Film>();
		Statement statement = connexion.createStatement();
		ResultSet films = statement.executeQuery("SELECT * FROM Film");
		while (films.next()) {
			Film film = new Film();
			film.setId(films.getInt("id"));
			film.setTitre(films.getString("titre"));
			film.setAnnee(films.getInt("annee"));
			film.setGenre(films.getString("genre"));
			film.setId_realisateur(films.getInt("id_realisateur"));
			film.setCode_pays(films.getString("code_pays"));
			resultat.add(film);
		}
		return resultat;
	}

	public Artiste chercheArtiste(Integer idArtiste) throws SQLException {
		Artiste resultat = new Artiste();
		PreparedStatement preparedStatement  = connexion.prepareStatement("SELECT * FROM Artiste WHERE id=?");
		preparedStatement.setInt( 1, idArtiste );
		ResultSet rs = preparedStatement.executeQuery();
		while(rs.next()) {
			resultat.setId(rs.getInt("id"));
			resultat.setNom(rs.getString("nom"));
			resultat.setPrenom(rs.getString("prenom"));
			resultat.setAnnee_naissance(rs.getInt("annee_naissance"));
		}
		return resultat;
	}
	
	public List<Film> chercheFilmD() throws SQLException {
		List<Film> resultat = new ArrayList<Film>();
		Statement statement = connexion.createStatement();
		ResultSet films = statement.executeQuery("SELECT * FROM Film");
		while (films.next()) {
			Film film = new Film();
			film.setId(films.getInt("id"));
			film.setTitre(films.getString("titre"));
			film.setAnnee(films.getInt("annee"));
			film.setGenre(films.getString("genre"));
			film.setId_realisateur(films.getInt("id_realisateur"));
			film.setCode_pays(films.getString("code_pays"));
			Artiste realisateur = chercheArtiste(film.getId_realisateur());
			film.setRealisateur(realisateur);
			resultat.add(film);
		}
		return resultat;
	}
	
}
