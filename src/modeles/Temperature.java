package modeles;

public class Temperature {
	
	private double celsius;
	
	private double fahrenheit;

	public Temperature(double valeurCelsius) {
		celsius = valeurCelsius;
	}
	
	public Temperature(double valeur, String unite) {
		if (unite=="C"){
			celsius = valeur;
			fahrenheit = (celsius * 9/5) + 32;
		} else if (unite=="F") {
			fahrenheit = valeur;
			celsius = (fahrenheit-32)*5/9;
		}

	}

	public double getCelsius() {
		return celsius;
	}
	
	public double getFahrenheit() {
		fahrenheit = (celsius * 9 / 5) +32;
		return fahrenheit;
	}
	
}
