package modeles;

public class Temperature2 {
	
	private double celsius;
	
	private double fahrenheit;

	public Temperature2(double valTemp, String unite) {
		if (unite=="C"){
			celsius = valTemp;
			fahrenheit = (celsius * 9/5) + 32;
		} else if (unite=="F") {
			fahrenheit = valTemp;
			celsius = (fahrenheit-32)*5/9;
		}

	}

	public double getCelsius() {
		return celsius;
	}
	
	public double getFahrenheit() {
		return fahrenheit;
	}
	

}
