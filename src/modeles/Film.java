package modeles;

public class Film {
	
	private Integer id;
	private String titre;
	private Integer annee;
	private Integer id_realisateur;
	private String genre;
	private String resume;
	private String code_pays;
	private Artiste realisateur;
	
	public Film() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public Integer getId_realisateur() {
		return id_realisateur;
	}

	public void setId_realisateur(Integer id_realisateur) {
		this.id_realisateur = id_realisateur;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getCode_pays() {
		return code_pays;
	}

	public void setCode_pays(String code_pays) {
		this.code_pays = code_pays;
	}

	public Artiste getRealisateur() {
		return realisateur;
	}

	public void setRealisateur(Artiste realisateur) {
		this.realisateur = realisateur;
	}

	
}
