<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>Convertisseur de température</title>
</head>
<body>

<p>Vous pouvez convertir une température exprimée en <b>Celsius</b> en une valeur exprimée en <b>Fahrenheit</b>.</p>
<form method="post" action = "${pageContext.request.contextPath}/Convertisseur2">
<label>Valeur en Celsius : </label><input type="text" size="20" name="celsius" /><br />
<hr />
<p>Ou bien convertir une température exprimée en <b>Fahrenheit</b> en une valeur exprimée en <b>Celsius</b>.</p>
<label>Valeur en Fahrenheit : </label><input type="text" size="20" name="fahrenheit" /><br />
<hr />
<input type="submit" value="Convertir" />
</form>

</body>
</html>