<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<p>Parametre 1 : ${param.p1}</p>
<%
            String parametre = request.getParameter( "p1" );
            out.println( parametre );
%>            
<p>Parametre 2 : ${param.p2}</p>
<p>Parametre 3 : ${param.p3}</p>

<c:out value="test" /><br>
<c:set var="uneVariable" value="${17*50}" />
<c:out value="${uneVariable}" /><br>
<c:if test="${uneVariable<10}">uneVariable vaut moins de 10</c:if><br>
<c:choose>
	<c:when test="${uneVariable==0}">Variable nulle</c:when>
	<c:when test="${uneVariable>0 && uneVariable<=10}">Variable superieure à 0</c:when>
	<c:when test="${uneVariable>10 && uneVariable<=100}">Variable superieure à 10</c:when>
	<c:when test="${uneVariable>100}">Variable superieure à 100</c:when>	
</c:choose>
<br>
<c:forEach items="${param}" var="par">
	<c:out value="${par.value}" /><br>
</c:forEach>

<table>
<c:forEach items="${header}" var="par">
	<tr>
		<td><c:out value="${par.key}" /></td>
		<td><c:out value="${par.value}" /></td>
	</tr>
</c:forEach>
</table>

</body>
</html>