<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Résultat de la conversion</title>
</head>
<body>

<p>Vous avez demandé la conversion en Celsius de la valeur en Fahrenheit ${requestScope.temperature.fahrenheit}</p>
<p><b>Le résultat est ${requestScope.temperature.celsius} degrés Celsius</b>! </p>


</body>
</html>