<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Résultat de la conversion</title>
</head>
<body>

<p>Vous avez demandeé la conversion en Fahrenheit de la valeur en Celsius ${requestScope.temperature.celsius}</p>
<p><b>Le résultat est ${requestScope.temperature.fahrenheit} degrés Fahrenheit</b>! </p>


</body>
</html>