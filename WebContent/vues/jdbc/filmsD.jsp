<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML>

<html>

<head>
<meta charset="UTF-8">
<title>Liste des films</title>
</head>

<body>

<h1>Liste  des films D</h1>
<ul>
<c:forEach items="${requestScope.films}" var="film" >
	<li>
	 <c:out value="${film.titre}" />
	 - <c:out value="${film.annee}" />
	 - <c:out value="${film.genre}" /> 
	 - <c:out value="${film.id}" /> 
	 - <c:out value="${film.id_realisateur}" />
	 - <c:out value="${film.realisateur.prenom}" />
	 - <c:out value="${film.realisateur.nom}" />
	 - <c:out value="${film.code_pays}" />
	 </li>
</c:forEach>
</ul>

<p><a href="${pageContext.request.contextPath}/jdbc">Accueil</a></p>

</body>


</html>