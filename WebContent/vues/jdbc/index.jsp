<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>

<html>

<head>
<meta charset="UTF-8">
<title>Menu JDBC</title>
</head>

<body>
<p>Parametre 1 : ${param.action}</p>
<h1>Actions JDBC</h1>
<ul>
<li><a href="${pageContext.request.contextPath}/jdbc?action=connexion">connexion</a></li>
<li><a href="${pageContext.request.contextPath}/jdbc?action=requeteA">RequeteA</a></li>
<li><a href="${pageContext.request.contextPath}/jdbc?action=requeteB">RequeteB</a></li>
<li><a href="${pageContext.request.contextPath}/jdbc?action=requeteC">RequeteC</a></li>
<li><a href="${pageContext.request.contextPath}/jdbc?action=artiste&id=1">Artiste</a></li>
<li><a href="${pageContext.request.contextPath}/jdbc?action=requeteD">RequeteD</a></li>
</ul>
</body>

</html>