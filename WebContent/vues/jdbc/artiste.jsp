<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<ul>
	<li><c:out value="${artiste.id}" /></li>
	<li><c:out value="${artiste.nom}" /></li>
	<li><c:out value="${artiste.prenom}" /></li>
	<li><c:out value="${artiste.annee_naissance}" /></li> 
</ul>

</body>
</html>